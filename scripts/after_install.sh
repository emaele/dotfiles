#!/bin/bash

# Update
sudo pacman -Syy

# Install packages
sudo pacman -S i3-gaps i3status network-manager-applet dmenu \
 gnome-control-center gnome-tweak-tool \
 telegram-desktop openssh ntfs-3g ttf-opensans neofetch wget curl go git

# Aur packages
mkdir aur
cd aur

# Google Chrome
echo "Google Chrome..."
git clone https://aur.archlinux.org/google-chrome.git
cd google-chrome
makepkg -si
cd ..

# Polybar
echo "Polybar..."
git clone https://aur.archlinux.org/polybar.git
cd polybar
makepkg -si
cd ..

# Siji font
echo "Siji font..."
git clone https://aur.archlinux.org/siji-git.git
cd siji-git
makepkg -si
cd ..

# VS Code
echo "Visual Studio Code..."
git clone https://aur.archlinux.org/visual-studio-code-bin.git
cd visual-studio-code-bin
makepkg -si
cd ..

# Paper Icons
echo "Paper Icons..."
git clone https://aur.archlinux.org/paper-icon-theme.git
cd paper-icon-theme
makepkg -si
cd ..

# Put config files
cd ~
cd .config
# i3
mkdir i3
cd i3
wget https://gitlab.com/emaele/dotfiles/raw/master/i3/config
cd ..
# polybar
mkdir polybar
cd polybar
wget https://gitlab.com/emaele/dotfiles/raw/master/polybar/config
# Exiting
cd ~
echo "Please reboot."